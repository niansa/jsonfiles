#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

#include <jsoncpp/json/json.h>


std::string readfile(std::string path) {
	std::stringstream fdata;
	std::ifstream file(path);
	fdata << file.rdbuf();
	file.close();
	return fdata.str();
}

int main(int argc, char** argv) {
    if (argc == 1) {
        std::cout << "{}" << std::flush;
        return 1;
    }
	Json::Value jsondata;
	int arg = 1;
	while (arg < argc) {
		jsondata[argv[arg]] = readfile(argv[arg]);
		arg++;
	}
	std::cout << jsondata << std::flush;
	return 0;
}
